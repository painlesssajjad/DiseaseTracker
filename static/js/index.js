const show = document.querySelector("#show");
const inputField = document.querySelector("#input");
const checkBtn = document.querySelector("#check");
const showing = document.querySelector("#showing");
const diseaseShowing = document.querySelector("#diseaseShowing");
let out = "";
let diseaseName;

async function fetchDataNow(dName, loc) {
    const diseaseApi = `https://disease.sh/v3/${dName}/${loc}`;
    await fetch(diseaseApi)
        .then(res => res.json())
        .then(data => showData(data))
}


async function showData(fetchedData){
    
    fetchedData = Object.entries(fetchedData);

    fetchedData.forEach(([key, value]) => {
        if(key !== "countryInfo" || key !== "updated" || key !== "countries") {
            out += `<p><b>${key}:</b> ${value}</p>`;
        }
    });
    if(out.length === 0 || out === ""){
     show.innerHTML = `<p class="text-danger p-3">Data Unavailable!</p>`;
    }else {
        show.innerHTML = out;
    }
    out = "";
    inputField.value = "";
}



document.addEventListener("DOMContentLoaded", () => {

    checkBtn.addEventListener("click", () => {
        show.innerHTML = "";
        let location = inputField.value.toLowerCase().split(", ");
        diseaseName = location[0];
        const continentOrCountry = location[1];
        location = location[2];

        showing.innerHTML = `${location.toUpperCase()}`;
        if(continentOrCountry === "all" || location === "all") {
            location = `all`;
        } else if ( continentOrCountry === "country" ) {
            location = `countries/${location}`;
        } else if ( continentOrCountry === "continent" ){
            location = `continents/${location}`;
        }

        diseaseShowing.innerHTML = `${diseaseName}`;

        fetchDataNow(diseaseName, location).then(r => r);

    });
});

